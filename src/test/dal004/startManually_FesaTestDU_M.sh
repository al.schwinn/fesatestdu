#!/bin/sh
set -u
set -e

# Automatically generated start script for FESA3 deploy-unit for manual testing and debugging only
# Please note that this script will be overwritten upon recreation.

print_help()
{ 
    echo -e "	Supported Arguments:"
    echo -e "		-h	Show this help"
    echo -e "		-g	launch the deploy-unit with GDB for debugging"
    echo -e "		-r	launch the deploy-unit for remote debugging"
    echo -e "		-c	launch as client in order to listen to an open remote debugging session ( script started with '-r' on FEC )"
    echo -e "		-f	pass a list of arguments to the deployment unit (use '-f -h' to see all possibilities"
    exit 1
}

CPU=x86_64                          # CPU architecture
FESA_VERSION=7.3.0
DU_NAME=FesaTestDU
EXEC_APPENDIX=_M         # used in order to differ between mixed / rt / server executables
GDB="false"                         # debugging via GDB
REMOTE="false"                      # remote debugging
REMOTE_CLIENT="false"                      # remote debugging client
ARG=""                              # optional arguments for the deploy-unit

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")     # path where this script is located in

# TODO: https://samindaw.wordpress.com/2009/02/25/how-to-findout-what-ports-are-free-using-a-linux-shell-script/
REMOTE_DEBUG_PORT=46697

while getopts ":f:grhc" opt; do
  case $opt in
  h)
    print_help
    ;;
  \?)
    echo "Invalid option: -$OPTARG" >&2 
    print_help
    ;;
  :)
    echo "Option -$OPTARG requires an argument." >&2 
    print_help
    ;;
  g)
    GDB="true"
    ;;
  r)
    REMOTE="true"
    ;;
  f)
    ARG=$OPTARG
    ;;
  c)
    REMOTE_CLIENT="true"
    ;;
  esac
done

# The default configuration
# works for starting the binary from a FEC (scu, scuxl, sddsc ) /opt/fesa/nfs/local/*
# works for starting the binary from */common/export/*
CONFIG_PATH=${SCRIPTPATH}
BINARY=${SCRIPTPATH}/${DU_NAME}
INSTANCE=${SCRIPTPATH}/${DU_NAME}.instance
FESA_CFG=${SCRIPTPATH}/fesa.cfg

# overwrite variables in case of starting the binary from the home folder
if [[ ${SCRIPTPATH} == */home/* ]]; then
    if [ -d "${HOME}/tmp/opt/fesa/fesa-fwk/${FESA_VERSION}/resources" ]; then
        CONFIG_PATH=${HOME}/tmp/opt/fesa/fesa-fwk/${FESA_VERSION}/resources
    else
        CONFIG_PATH=/common/export/fesa/global/etc/fesa/${FESA_VERSION}
    fi
    BINARY=../../../build/bin/${CPU}/${DU_NAME}${EXEC_APPENDIX}
    INSTANCE=${SCRIPTPATH}/DeviceData_${DU_NAME}.instance
    FESA_CFG=${CONFIG_PATH}/fesa.aslCluster.cfg
fi

# Special case: If we start a delivered binary on asl/vml, we dont use the delivered fesa.cfg, but the global one
# That's because the delivered fesa.cfg only works if the binary is started with root privileges
if [[ ${SCRIPTPATH} == */common/export/* ]]; then
    FESA_CFG=/common/export/fesa/global/etc/fesa/${FESA_VERSION}/fesa.aslCluster.cfg
fi

LOG_CFG=${CONFIG_PATH}/log.cfg
CMW_CFG=${CONFIG_PATH}/cmw.cfg
MSG_CFG=${CONFIG_PATH}/messages.cfg
MSG_GSI_CFG=${CONFIG_PATH}/messagesLab.cfg

# SAFTlib library path for asl/vml
export LD_LIBRARY_PATH=/common/export/timing-rte/tg-fallout-v6.2.0/x86_64/lib/:${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}

# default CMW directory server, environment variable needs to be adjusted for test/production
export CMW_DIRECTORY_CLIENT_SERVERLIST=cmwdev00a.acc.gsi.de:5021

# check if a daemon already runs the binary
PROCESS_NUM=$(ps a | grep du/${DU_NAME}/${DU_NAME} | grep du/${DU_NAME}/${DU_NAME}.instance | grep -v "grep" | wc -l)
if [ $PROCESS_NUM -gt 0 ]; then
    echo "Error: The FESA binary du/${DU_NAME}/${DU_NAME} using the instance file: du/${DU_NAME}/${DU_NAME}.instance is already running as a daemon service"
    exit 1
fi

# check if the binary already was launched manually
PROCESS_NUM=$(ps a | grep ${BINARY} | grep ${INSTANCE} | grep -v "grep" | wc -l)
if [ $PROCESS_NUM -gt 0 ]; then
    echo "Error: The FESA binary ${BINARY} using the instance file: ${INSTANCE} was already started"
    exit 1
fi

ARGUMENTS="-cfgcmw ${CMW_CFG} -cfglog ${LOG_CFG} -cfgfesa ${FESA_CFG} -cfgmsg ${MSG_CFG} -cfgmsglab ${MSG_GSI_CFG} -instance ${INSTANCE}"
echo -e "
application arguments:"
echo -e "-cfgcmw ${CMW_CFG}
-cfglog ${LOG_CFG}
-cfgfesa ${FESA_CFG}
-cfgmsg ${MSG_CFG}
-cfgmsglab ${MSG_GSI_CFG}
-instance ${INSTANCE}
${ARG}"

if [ "${GDB}" == "true" ]; then
    echo -e "Launching FESA software with:"
    echo -e "gdb -ex "break +2" -ex "run" --args ${BINARY} ${ARGUMENTS} ${ARG}"
    echo "- break at first line in main"
    echo "- run gdb with binary + arguments"
    gdb -ex "break +2" -ex "run" --args ${BINARY} ${ARGUMENTS} ${ARG}
elif [ "${REMOTE}" == "true" ]; then
    echo -e "Launching FESA software as GDB server:"
    echo -e "gdbserver localhost:${REMOTE_DEBUG_PORT} ${BINARY} ${ARGUMENTS} ${ARG}"
    gdbserver localhost:${REMOTE_DEBUG_PORT} ${BINARY} ${ARGUMENTS} ${ARG}
elif [ "${REMOTE_CLIENT}" == "true" ]; then
    FEC_NAME=${PWD##*/}
    echo -e "Launching FESA software as remote client for GDB:"
    echo -e "gdb -ex "target remote ${FEC_NAME}:${REMOTE_DEBUG_PORT}" -s ${BINARY}"
    gdb -ex "target remote ${FEC_NAME}:${REMOTE_DEBUG_PORT}" -s ${BINARY}
else
    echo -e "Launching FESA software with:"
    echo -e "${BINARY} ${ARGUMENTS} ${ARG}"
    exec ${BINARY} ${ARGUMENTS} ${ARG}
fi
