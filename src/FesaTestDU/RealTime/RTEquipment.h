#ifndef FesaTestDU_RT_EQUIPMENT_H_
#define FesaTestDU_RT_EQUIPMENT_H_

#include "FesaTestDU/GeneratedCode/RTEquipmentGen.h"
#include <fesa-core/RealTime/AbstractEventSource.h>
#include <fesa-core/RealTime/RTScheduler.h>

namespace FesaTestDU
{

class RTEquipment : public RTEquipmentGen
{
	public:

		 RTEquipment (const std::map<std::string, fesa::AbstractServiceLocator*>& serviceLocatorCol);

		virtual ~RTEquipment ();

		void specificInit();

		/*!
		* \brief this method is called when an error occurs in a AbstractEventSource
		* \param eventSource in which the error occurred
		* \param exception with the error code and error description
		*/
		void handleEventSourceError (fesa::AbstractEventSource* eventSource,fesa::FesaException& exception);

		/*!
		* \brief this method is called when an error occurs in a AbstractEventSource
		* \param eventSource in which the error occurred
		* \param exception with the error code and error description
		*/
		void handleSchedulerError (fesa::RTScheduler* scheduler,fesa::FesaException& exception);
};
}
#endif 
