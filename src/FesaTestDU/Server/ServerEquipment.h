#ifndef FesaTestDU_SERVER_EQUIPMENT_H_
#define FesaTestDU_SERVER_EQUIPMENT_H_

#include <fesa-core/Server/AbstractServerEquipment.h>

#include <fesa-core/Exception/FesaException.h>

#include <FesaTestDU/GeneratedCode/ServerEquipmentGen.h>

namespace FesaTestDU
{

class ServerEquipment : public ServerEquipmentGen
{
	public:

		ServerEquipment (const std::map<std::string, fesa::AbstractServiceLocator*>& serviceLocatorCol);

		virtual  ~ServerEquipment();

		void specificInit();

};
}
#endif
